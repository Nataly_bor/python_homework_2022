# Візьміть свою гру з HW8 і перепишіть ії в обʼєктному стилі.
# Зробіть максимум взаємодії (як явної так і неявної) на рівні обʼєктів.
# Рекомендую подумати над такими класами як Player, GameFigure, Game.
# Памʼятайте про чистоту і простоту коду (DRY, KISS), коментарі та докстрінги.

from random import randint

list_choice = ['rock', 'paper', 'scissors', 'lizard', 'spock']

class Player:
	def __init__(self):
		self.choice = None
		self.score = 0

	def get_input(self, choice, run):
		"""
		Assign a value in the list of choises to the variable 'choice' of a player
		Args:
			choice: int
			run: [0, 1]

		Returns: [0, 1]

		"""
		z = 0
		for x in range(5):
			if choice == list_choice[x]:
				z = 1
		if z == 0:
			print('Wrong input')
			run = False
			return run
		if z == 1:
			print('Input accepted')
			self.choice = choice
			return run

	def return_choice(self):
		return self.choice


	def check(self, computer_choice):
		"""
		Rules to define the winner of the game
		Args:
			computer_choice: str

		Returns: None

		"""
		if self.choice != 'None':
			if self.choice == 'rock' and computer_choice == 'scissors':
				self.score = self.score + 1
			elif self.choice == 'rock' and computer_choice == 'paper':
				self.score = self.score - 1
			elif self.choice == 'rock' and computer_choice == 'lizard':
				self.score = self.score + 1
			elif self.choice == 'rock' and computer_choice == 'spock':
				self.score = self.score - 1
			if self.choice == 'paper' and computer_choice == 'rock':
				self.score = self.score + 1
			elif self.choice == 'paper' and computer_choice == 'scissors':
				self.score = self.score - 1
			elif self.choice == 'paper' and computer_choice == 'lizard':
				self.score = self.score - 1
			elif self.choice == 'paper' and computer_choice == 'spock':
				self.score = self.score + 1
			if self.choice == 'scissors' and computer_choice == 'paper':
				self.score = self.score + 1
			elif self.choice == 'scissors' and computer_choice == 'rock':
				self.score = self.score - 1
			elif self.choice == 'scissors' and computer_choice == 'lizard':
				self.score = self.score + 1
			elif self.choice == 'scissors' and computer_choice == 'spock':
				self.score = self.score - 1
			if self.choice == 'lizard' and computer_choice == 'spock':
				self.score = self.score + 1
			elif self.choice == 'lizard' and computer_choice == 'lizard':
				self.score = self.score - 1
			elif self.choice == 'lizard' and computer_choice == 'paper':
				self.score = self.score + 1
			elif self.choice == 'lizard' and computer_choice == 'rock':
				self.score = self.score - 1
			if self.choice == 'spock' and computer_choice == 'spock':
				self.score = self.score + 1
			elif self.choice == 'spock' and computer_choice == 'lizard':
				self.score = self.score - 1
			elif self.choice == 'spock' and computer_choice == 'rock':
				self.score = self.score + 1
			elif self.choice == 'spock' and computer_choice == 'paper':
				self.score = self.score - 1


	def return_score(self):
		return self.score

player = Player()
run = True
while run:
	run = player.get_input(input('Enter your choice: '), run)
	if run == True:
		computer_choice = list_choice[randint(0, 4)]
		print(f'The computer choice is {computer_choice}')
		player.check(computer_choice)
	print(f'You score is {player.return_score()}')
