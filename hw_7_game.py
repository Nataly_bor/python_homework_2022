# Напишіть гру "rock scissors paper lizard spock".
# Використайте розділення всієї програми на функції (Single-Responsibility principle).
# Як скелет-заготовку можна використати приклад з заняття.
# До кожної функції напишіть докстрінг або анотацію

def choice():
    """
    Ця фукнкція приймає вибір гравця, генерує рендомний вібір компьютера,
    і повертає їх як значення функціїї у вигляді tuple
  Returns:
          (tuple):
    """
    while True:
        import random
        player1_choice = input('Your choice?\n r for Rock\n s for scissors\n p for paper\n l for lizard\n'
                               ' o for Spock\n ---> ')
        list_play = ['r', 's', 'p', 'l', 'o']
        # rock scissors paper lizard spock
        if player1_choice.lower() in list_play:
            player2_choice = random.choice(list_play)
            return player1_choice.lower(), player2_choice
        else:
            print('Choose from r, s, p, l, o')


rules = {
    'r': ['s', 'l'],
    's': ['p', 'l'],
    'p': ['r', 'o'],
    'l': ['o', 'p'],
    'o': ['s', 'r'],
      }


def winner(arg1, arg2):
    """
Ця функція визначає результат раунду гри, який може мати три варіанти: win, loos, tie
    Args:
        arg1: (tuple):
        arg2: (dict):

    Returns:
        (str)
    """
    if arg1[0] == arg1[1]:
        return 'tie'
    elif arg1[1] in arg2[arg1[0]]:
        return 'win'
    else:
        return 'loos'


def naming(arg1, arg2):
    """
    Функція перетворює короткі позначення фігур у їх повні імена
    Args:
        arg1: (str):
        arg2: (dict):

    Returns:
        (str):
    """
    return arg2.get(arg1)


counter_tamplate = {'games': 0, 'win': 0, 'loos': 0, 'tie': 0, }


def count(arg, dic_counter):
    """
    Ця функція збільшує на одиницю відповідний лічильник в дікті count в залежності від результату раунду гри
    Args:
        arg: (str):
        dic_counter: (dict)

    Returns:
        (None):
    """
    dic_counter[arg] = dic_counter[arg]+1
    dic_counter['games'] = dic_counter['win'] + dic_counter['loos'] + dic_counter['tie']


def result(arg1, arg2):
    """
    Функція виводить вибір гравця, вибір компьютера і результат раунда гри
    Args:
        arg1: (tuple):
        arg2: (str):

    Returns:
        (None):
    """
    names = {'r': 'rock', 's': 'scissors', 'p': 'paper', 'l': 'lizard', 'o': 'Spock', }
    name1 = naming(arg1[0], names)
    name2 = naming(arg1[1], names)
    print(f'You choose {name1}\nComputer\'s choice is {name2} \n You {arg2}')


def game_result(d: dict):
    """
    Фунцкія виводить результат усієї гри: кількість зіграних раундів, кількість раундів з результатом
    win, loos, tie
    Args:
        d:
         (dict):
    Returns:
        (None):
    """
    print(f'You played {d["games"]} rounds')
    print(f'You won {d["win"]} rounds')
    print(f'You loos {d["loos"]} rounds')
    print(f'{d["tie"]} rounds with result tie')


def game():
    """
    Функція запускає цикл гри та дозволяє обрати можливість продовжити або перервати гру.
    Також віводить результат серії ігор
    Returns:
        (None):

    """
    print('<-----Game start---->\n'
          '<------------------>')
    counter = dict(counter_tamplate)
    while True:
        choice_tuple = choice()
        result(choice_tuple, winner(choice_tuple, rules))
        count(winner(choice_tuple, rules), counter)
        play_again = input('Play again? y for Yes! n for No\n---> ')
        if play_again.lower() != 'y':
            print('<-----Game result---->')
            game_result(counter)
            break


game()
