# Створіть клас, який реалізує підключення до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ). Обʼєкт класу повинен вміти отримувати курс валют станом на певну дату. Обʼєкт класу повинен вміти записати курси в текстовий файл. Назва файлу повинна містити дату на яку шукаємо курс, наприклад:
#  21_11_2019.txt
# Дані в файл запишіть у вигляді списку :
# 1. [назва валюти 1] to UAH: [значення курсу до валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу до валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу до валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу до валюти n]
#
# P.S. Архітектура класу - на розсуд розробника. Не забувайте про DRY, KISS, YAGNI, SRP та перевірки!)


class Currency:
	_nbu_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json'
	_date = None

	def __init__(self, date):
		self.filename = '_'.join(date.split('.')) + '.txt'
		self.date = date
		self.url = self._nbu_url + '&date=' + self.date
		self.my_response = self.response
		self.my_currency_file = self.currency_file

	@property
	def date(self):
		return self._date

	@date.setter
	def date(self, date):
		print('Date here: ', date)
		from datetime import datetime
		try:
			date_object = datetime.strptime(date, '%d.%m.%Y').date()
		except Exception as e:
			print('Exception: ', e)
		else:
			if date_object <= datetime.today().date():
				year_string = str(date_object.year)
				month_string = str(date_object.month)
				day_string = str(date_object.day)
				if len(month_string) == 1:
					month_string = '0' + month_string
				if len(day_string) == 1:
					day_string = '0' + day_string
				date_string = year_string + month_string + day_string
				self._date = date_string
			else:
				print('You need to initiate the Currency object with date not later than today')


	@property
	def response(self):
		import requests
		try:
			response = requests.get(self.url)
		except Exception as e:
			print(e)
		else:
			if 300 > response.status_code >= 200:
				if "application/json" in response.headers.get('Content-Type', ''):
					return response.json()
				else:
					print('None json type of content')

	@property
	def currency_file(self):
		counter = 0
		data_string = ''
		data_frame = ''
		for d in self.my_response:
			counter = counter + 1
			if isinstance(d, dict):
				if "txt" in d.keys() and 'rate' in d.keys():
					data_frame = '#' + str(counter) + ' ' + d.get('txt') + ' to UAN: ' + str(
						round(1 / d.get('rate'), 4)) + '\n'
				else:
					print('The needed keys are not in dictionaries')
			else:
				print('Inproper type of the element of json-file')
			data_string = data_string + data_frame
		with open(self.filename, 'w') as file:
			file.write(data_string)

cur = Currency('04.01.2023')
