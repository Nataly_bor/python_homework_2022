#1. Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість
# слів,які містять дві голосні літери підряд.
print('Task 1')
my_sentence = input('Type in a sentence: ')
my_sentence_lst = my_sentence.split()
words_numbers = 0
vowels = ('a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U', 'y', 'Y')
for word in my_sentence_lst:
    for i in range(0,len(word) - 1):
         if word[i] in vowels and word[i+1] in vowels:
            words_numbers += 1
            break
print(f'There are {words_numbers} words with adjacent vowels')

# 2. Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166,
# "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"
print('Task 2')
shops = {"cito": 47.999, "BB_studio" : 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166,
"the_partner": 38.988, "store": 37.720, "rozetka": 38.003, }
try:
    lower_limit = float(input('Type in lower limit: '))
    upper_limit = float(input('Type in upper limit: '))
except:
    print('Please, type in numbers')
else:
    appropriate_shops = []
    for shop_name, price in shops.items():
        if price >= lower_limit and price <= upper_limit:
            appropriate_shops.append(shop_name)
    if len(appropriate_shops) > 0:
        print('match: ', end='')
        for name in appropriate_shops:
            print(name, ", ", end='')
    else:
        print('There are no appropriate items')



