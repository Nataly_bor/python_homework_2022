import random


def decorator_save_game_result_to_file(file_name):
    """
    The function is a decorator to record the result of the game to a separate file
    Args:
        file_name: (txt):
    Returns:
            (None):
    """

    def decorator(function):
        def wrapper(*args):
            result = function(*args)
            with open(file_name, 'a') as file:
                file.write(f'Game result: {args}\n')
            return result
        return wrapper
    return decorator


def choice():
    """
    This function takes the player's choices, generates a random computer vibe,
    and returns them as function values in the tuple format
  Returns:
          (tuple):
    """
    while True:
        player1_choice = input('Your choice?\n r for Rock\n s for scissors\n p for paper\n l for lizard\n'
                               ' o for Spock\n ---> ')
        list_play = ['r', 's', 'p', 'l', 'o']
        # rock scissors paper lizard spock
        if player1_choice.lower() in list_play:
            player2_choice = random.choice(list_play)
            return player1_choice.lower(), player2_choice
        else:
            print('Choose from r, s, p, l, o')


rules = {
    'r': ['s', 'l'],
    's': ['p', 'l'],
    'p': ['r', 'o'],
    'l': ['o', 'p'],
    'o': ['s', 'r'],
      }


def winner(arg1, arg2):
    """
This function determines the result of the game round, which can have three options: win, loose, tie
    Args:
        arg1: (tuple):
        arg2: (dict):

    Returns:
        (str)
    """
    if arg1[0] == arg1[1]:
        return ['tie', arg1[0], arg1[1]]
    elif arg1[1] in arg2[arg1[0]]:
        return ['win', arg1[0], arg1[1]]
    else:
        return ['loos', arg1[1], arg1[0]]


def naming(arg1, arg2):
    """
    The function converts the short symbols of figures into their full names
    Args:
        arg1: (str):
        arg2: (dict):

    Returns:
        (str):
    """
    return arg2.get(arg1)


counter_template = {'games': 0, 'win': 0, 'loos': 0, 'tie': 0, }


def count(arg, dic_counter):
    """
    This function increases the corresponding counter in the count dict depending on the result of the game round
    Args:
        arg: (str):
        dic_counter: (dict):

    Returns:
        (None):
    """
    dic_counter[arg] = dic_counter[arg]+1
    dic_counter['games'] = dic_counter['win'] + dic_counter['loos'] + dic_counter['tie']


def result(arg1, arg2):
    """
    The function outputs the player's choice, the computer's choice and the result of the game round
    Args:
        arg1: (tuple):
        arg2: (str):

    Returns:
        (None):
    """
    names = {'r': 'rock', 's': 'scissors', 'p': 'paper', 'l': 'lizard', 'o': 'Spock', }
    name1 = naming(arg1[0], names)
    name2 = naming(arg1[1], names)
    print(f'You choose {name1}\nComputer\'s choice is {name2} \n You {arg2}')


@decorator_save_game_result_to_file('Rock_scissors_paper_results.txt')
def game_result(d: dict):
    """
    The function displays the result of the entire game: the number of rounds played, the number of rounds with a result
     win, lose, tie
    Args:
        d: (dict):
    Returns:
        (None):
    """
    print(f'You played {d["games"]} rounds')
    print(f'You won {d["win"]} rounds')
    print(f'You loos {d["loos"]} rounds')
    print(f'{d["tie"]} rounds with result tie')


counter_figures_template = {'r': 0, 's': 0, 'p': 0, 'l': 0, 'o': 0, }


def count_figures(arg1, arg2):
    """
    The function calculates the number of times that every figures played
    Args:
        arg1: (list):
        arg2: (dict):

    Returns:
            (None):
    """
    arg2[arg1[1]] = arg2[arg1[1]]+1
    arg2[arg1[2]] = arg2[arg1[2]]+1


@decorator_save_game_result_to_file('Rock_scissors_paper_results.txt')
def figures_result(d: dict):
    """
    The function displays the result of the entire game: the number of rounds played, the number of rounds with a result
     win, lose, tie
    Args:
        d:
         (dict):
    Returns:
        (None):
    """
    print(f'Rock played {d["r"]} times')
    print(f'Scissors played {d["s"]} times')
    print(f'Paper played {d["p"]} times')
    print(f'Lizard played {d["l"]} times')
    print(f'Spock played {d["o"]} times')


def game():
    """
    The function starts the game cycle and allows to choose whether to continue or interrupt the game.
     Also displays the result of a series of games
    Returns:
        (None):

    """
    print('<-----Game start---->\n'
          '<------------------>')
    counter = dict(counter_template)
    counter_figures = dict(counter_figures_template)
    while True:
        choice_tuple = choice()
        result(choice_tuple, winner(choice_tuple, rules)[0])
        count(winner(choice_tuple, rules)[0], counter)
        count_figures(winner(choice_tuple, rules), counter_figures)
        play_again = input('Play again? y for Yes! n for No\n---> ')
        if play_again.lower() != 'y':
            print('<-----Game result---->')
            game_result(counter)
            figures_result(counter_figures)
            break

