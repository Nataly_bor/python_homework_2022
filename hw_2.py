#Напишіть программу "Касир в кінотеватрі", яка буде виконувати наступне:

#Попросіть користувача ввести свсвій вік (можно використати константу або input()).
#- якщо користувачу менше 7 - вивести повідомлення "Де твої батьки?"
#- якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
#- якщо користувачу більше 65 - вивести повідомлення "Покажіть пенсійне посвідчення!"
#- якщо вік користувача з двох однакових цифр - вивести повідомлення "Як цікаво!"
#- у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"

#P.S. На екран має бути виведено лише одне повідомлення, якщо вік користувача з двох однакових
#цифр то має бути виведено тільки відповідне повідомлення!
#Також подумайте над варіантами, коли введені невірні або неадекватні дані.

age_str = input('How old are you? \n')
if age_str.isdigit():
    age = int(age_str)
    if len(age_str) == 2:
        if age_str[0] == age_str[1]:
            print('It is so interesting')
        elif age >= 16 and age <= 65:
            print('Unfortunaterly, there are no tickets anymore')
        elif age >= 10 and age < 16:
            print('This movie is for adults only')
        elif age >= 65 and age <= 100:
         print('Could you please show me your pension card?')
        else:
            print('You might be joking')
    elif len(age_str) >= 1:
        if age >= 3 and age < 7:
             print('Were are your parents?')
        elif age >= 7 and age < 10:
            print ('This movie is for adults only')
        else:
         print('You might be joking')
else:
    print('Non correct data')