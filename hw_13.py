# Доопрацюйте всі перевірки на типи даних (x, y в Point, begin, end в Line, etc) - зробіть перевірки
# за допомогою property або класса-дескриптора.
# Доопрацюйте класс Triangle з попередньої домашки наступним чином:
# обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
# перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3
# print(str(triangle1))
# > (1,0 -- 5,9 -- 3,3)

class Figure:

	def area(self):
		return self._area()

	def length(self):
		return self._length()

	def _area(self):
		raise NotImplementedError

	def _length(self):
		raise NotImplementedError


class Point(Figure):
	_x = None
	_y = None

	def __init__(self, x, y):
		try:
			assert (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float))
			self.x = x
			self.y = y
		except:
			print('TypeError in an argument of Point')

		@property
		def x(self):
			"""
			displays the value of x-coordinate
			Args:
				self: object

			Returns: None

			"""
			return self._x

		@x.setter
		def x(self, x):
			"""
			checks the type and assign the value of x-coordinate
			Args:
				self: object
				x: int or float

			Returns:

			"""
			try:
				assert (isinstance(x, int) or isinstance(x, float))
				self._x = x
			except:
				print('TypeError in Point')

		@property
		def y(self):
			"""
						displays the value of y-coordinate
						Args:
							self: object

						Returns: None

						"""
			return self._y

		@y.setter
		def y(self, y):
			"""
						checks the type and assign the value of y-coordinate
						Args:
							self: object
							y: int or float

						Returns:

						"""
			try:
				assert (isinstance(y, int) or isinstance(y, float))
				self._y = y
			except:
				print('TypeError in Point')


class Line(Figure):
	_begin = None
	_end = None

	def __init__(self, begin, end):
		try:
			assert isinstance(begin, Point) and isinstance(end, Point)
			self.begin = begin
			self.end = end
		except:
			print('TypeError in Line')


	@property
	def begin(self):
		"""
		displays the point of the line beginning
		Returns: None

		"""
		return self._begin

	@begin.setter
	def begin(self, begin):
		"""
		checks the type and assign the point of the lign beginning
		Args:
			begin: Point

		Returns: None

		"""
		try:
			assert isinstance(begin, Point)
			self._begin = begin
		except:
			print('TypeError in Line')

		@property
		def end(self):
			"""
			displays the point of the lign ending
			Args:
				self:

			Returns: None

			"""
			return self._end

		@end.setter
		def end(self, end):
			"""
			checks the type and assign the point of the lign ending
			Args:
				self: object
				end: Point

			Returns: None

			"""
			try:
				assert isinstance(end, Point)
				self._end = end
			except:
				print('TypeError in Line')

	def length(self):
		"""
		calculates the length of the line
		Returns: float

		"""
		try:
			len = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
			return len
		except:
			print('Line building error')


class Triangle(Figure):
	_a = None
	_b = None
	_c = None
	_area = None

	def __init__(self, a, b, c):
		try:
			assert isinstance(a, Line) and isinstance(b, Line) and isinstance(c, Line)
			# self.a = a.length()
			# self.b = b.length()
			# self.c = c.length()
			self.a = a
			self.b = b
			self.c = c
		except:
			print('TypeError of an argument in Triangle')



	@property
	def a(self):
		"""
		displays the a-side of the triangle
		Returns: Line

		"""
		return self._a

	@a.setter
	def a(self, a):
		"""
		checks the type and assign the value to the a-side of the triangle
		Args:
			a: Line

		Returns: None

		"""
		try:
			assert isinstance(a, Line)
			#self._a = a.length()
			self._a = a
		except:
			print('TypeError in Triangle')

	@property
	def b(self):
		"""
				displays the b-side of the triangle
				Returns: Line

				"""
		return self._b

	@b.setter
	def b(self, b):
		"""
				checks the type and assign the value to the b-side of the triangle
				Args:
					b: Line

				Returns: None

				"""
		try:
			assert isinstance(b, Line)
			#self._b = b.length()
			self._b = b
		except:
			print('TypeError in Triangle')

	@property
	def c(self):
		"""
				displays the c-side of the triangle
				Returns: Line

				"""
		return self._c

	@c.setter
	def c(self, c):
		"""
				checks the type and assign the value to the c-side of the triangle
				Args:
					c: Line

				Returns: None

				"""
		try:
			assert isinstance(c, Line)
			#self._c = c.length()
			self._c = c
		except:
			print('TypeError in Triangle')

	def area(self):
		"""
		calculates the area of the triangla
		Returns: float
		"""
		try:
			# sp = (self.a + self.b + self.c) * 0.5
			# self._area = (sp * (sp - self.a) * (sp - self.b) * (sp - self.c)) ** 0.5
			a_len = self.a.length()
			b_len = self.b.length()
			c_len = self.c.length()
			sp = (a_len + b_len + c_len) * 0.5
			self._area = (sp * (sp - a_len) * (sp - b_len) * (sp - c_len)) ** 0.5

		except:
			print('Triangle building error')
		else:
			return self._area

	def __eq__(self, other):
		"""
		checks to objects on equality
		Args:
			other: object
		Returns: bool
		"""
		try:
			isinstance(other, type(self))
			return self._area == other._area
		except Exception:
			print(f'The type of {other} is not of {type(self).__name__}')

	def __ne__(self, other):
		"""
		checks to objects on inequality
		Args:
			other: object
		Returns: bool
		"""
		try:
			isinstance(other, type(self))
			return self._area != other._area
		except:
			print(f'The type of {other} is not of {type(self).__name__}')

	def __lt__(self, other):
		"""
		checks if the object is greater than the other
		Args:
			other: object
		Returns: bool
		"""
		try:
			isinstance(other, type(self))
			return self._area < other._area
		except:
			print(f'The type of {other} is not of {type(self).__name__}')

	def __gt__(self, other):
		"""
		checks if the object is lesser than the other

		Args:
			other: object
		Returns: bool
		"""
		try:
			isinstance(other, type(self))
			return self._area > other._area
		except:
			print(f'The type of {other} is not of {type(self).__name__}')

	def __str__(self):
		return f'{self.a.begin.x}, {self.a.begin.y} --- {self.b.begin.x}, {self.b.begin.y} --- {self.c.begin.x}, {self.c.begin.y}'

p1 = Point(0, 0)
p2 = Point(0, 5)
p3 = Point(4, 0)

a = Line(p1, p2)
b = Line(p2, p3)
c = Line(p3, p1)

my_triangle = Triangle(a, b, c)
print('my_triangle area is: ', my_triangle.area())

p11 = Point(1, 1)
p22 = Point(1, 8)
p33 = Point(6, 1)

a1 = Line(p11, p22)
b1 = Line(p11, p33)
c1 = Line(p22, p33)

my_triangle1 = Triangle(a1, b1, c1)
print('my_triangle1 area is: ', my_triangle1.area())
false_triangle = 5 #initializing an object of type int to mimic Triangle object for testing purpose
false_triangle1 = Triangle(a1, b1, 10) #testing raising exceptin in Troangle initiation with inproper argument

print('my_triangle == my_triangle1: ', my_triangle == my_triangle1)
print('my_triangle == false_triangle: ', my_triangle == false_triangle) #testing raising exception with comparing a triangle object to a unproper object type

print('my_triangle != my_triangle1: ', my_triangle != my_triangle1)
print('my_triangle < my_triangle1: ', my_triangle < my_triangle1)
print('my_triangle > my_triangle1: ', my_triangle > my_triangle1)

print(str(my_triangle))
