freeze_req:
	pip3 freeze > requirements.txt


setup:
	@echo 'setup has been started'
	pip3 install -r requirements.txt
	@echo 'setup has been finished'


check: setup
	mypy hw_10.py
	flake8 --ignore=E501 hw_10.py


test:
	pytest


run: setup test
	python3 hw_10.py