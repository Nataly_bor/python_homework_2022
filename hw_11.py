#Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

class Vehicle:
	def __init__(self, brand, model, year, max_speed, passanger_capacity, load_capacity):
		self.brand = brand
		self.model = model
		self.year = year
		self.max_speed = max_speed
		self.passanger_capacity = passanger_capacity
		self.load_capacity = load_capacity


class Car(Vehicle):
	def __init__(self,  brand, model, year, max_speed, passanger_capacity, load_capacity, car_mileage):
		super().__init__(brand, model, year, max_speed, passanger_capacity, load_capacity)
		self.car_mileage = car_mileage


class Plane(Vehicle):
	def __init__(self, brand, model, year, max_speed, passanger_capacity, load_capacity, number_of_flights):
		super().__init__(brand, model, year, max_speed, passanger_capacity, load_capacity)
		self.number_of_flights = number_of_flights


class Ship(Vehicle):
	def __init__(self, brand, model, year, max_speed, passanger_capacity, load_capacity, ship_type):
		super().__init__(brand, model, year, max_speed, passanger_capacity, load_capacity)
		self.ship_type = ship_type


vehicle_1 = Car('Ford', 'Kuga', 2022, 170, 5, 1, 1000)
vehicle_2 = Car('Audi', 'A8', 2021, 190, 5, 1, 150000)
car_colection = [vehicle_1, vehicle_2]
for i in car_colection:
	print(f"The car model is {i.brand} {i.model}, {i.year}. Passangers capacity is"
	  f" {i.passanger_capacity}, the load capacity is {i.load_capacity} ton, the mileage is "
	  f"{i.car_mileage} km")

vehicle_3 = Plane('Boing', 777, 2015, 790, 550, 800, 1187)
vehicle_4 = Plane('Airbus', 'A380', 2021, 650, 300, 700, 150)
plane_colection = [vehicle_3, vehicle_4]
for i in plane_colection:
	print(f"The plane model is {i.brand} {i.model}, {i.year}. Passangers capacity is"
	  f" {i.passanger_capacity}, the load capacity is {i.load_capacity} ton, the number of flights is "
	  f"{i.number_of_flights}")

vehicle_5 = Ship('Swan Hunter', 'Noosphere', 1990, 12, 200, 5372, 'icebreaker')
vehicle_6 = Ship('HII', 'USS America', 2012, 22, 3000, 45000, 'amphibious assault ship')
ship_colection = [vehicle_5, vehicle_6]
for i in ship_colection:
	print(f"The ship model is {i.brand} {i.model}, {i.year}. The speed is {i.max_speed} miles per hour. Passangers capacity is"
	  f" {i.passanger_capacity}, the load capacity is {i.load_capacity} ton, the ship tipe is "
	  f"{i.ship_type}")