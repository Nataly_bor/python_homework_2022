import functools

@functools.cache
def power_of_product(factor1: float, factor2: float, /, *, power: float = 1) -> float:
    result = (factor1 * factor2)**power
    return result
#print(power_of_product(1, 2, power=3))