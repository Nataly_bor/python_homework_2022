# Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу int або float
# Доопрацюйте класс Line так, щоб в атрибути begin та end обʼєктів цього класу можна було записати тільки обʼєкти класу Point
# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point). Реалізуйте перевірку даних,
# аналогічно до класу Line. Визначет метод, що містить площу трикутника. Для обчислень можна використати формулу Герона
# (https://en.wikipedia.org/wiki/Heron%27s_formula)

class Figure:

    def area(self):
        return self._area()

    def length(self):
        return self._length()

    def _area(self):
        raise NotImplementedError

    def _length(self):
        raise NotImplementedError


class Point(Figure):
    x = None
    y = None

    def __init__(self, x, y):
        try:
            assert (isinstance(x, int) or isinstance(x, float)) and (isinstance(y, int) or isinstance(y, float))
            self.x = x
            self.y = y
        except:
            print('TypeError in Point')


class Line(Figure):
    begin = None
    end = None

    def __init__(self, begin, end):
        try:
            assert isinstance(begin, Point) and isinstance(end, Point)
            self.begin = begin
            self.end = end
        except:
            print('TypeError in Line')

    def length(self):
        """
        calculates the length of triangle's side
        Returns: float

        """
        try:
            len = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
            return len
        except:
            print('Line building error')


class Triangle(Figure):
    a = None
    b = None
    c = None

    def __init__(self, a, b, c):
        try:
            assert isinstance(a, Line) and isinstance(b, Line) and isinstance(c, Line)
            self.a = a.length()
            self.b = b.length()
            self.c = c.length()
        except:
            print('TypeError in Triangle')

    def area(self):
        """
        calculates the area of triangle
        Returns:

        """
        try:
            sp = (self.a + self.b + self.c) * 0.5
            result = (sp * (sp - self.a) * (sp - self.b) * (sp - self.c)) ** 0.5
            return result
        except:
            print('Triangle building error')


p1 = Point(0, 0)
p2 = Point(0, 5)
p3 = Point(4, 0)


a = Line(p1, p2)
b = Line(p1, p3)
c = Line(p2, p3)


my_triangle = Triangle(a, b, c)
print(my_triangle.area())
