from library import power_of_product
import pytest

def test_type_power_of_product():
    assert type(power_of_product(1, 2, power=3)) == int, 'notInt'

def test_type_power_of_product_2():
    with pytest.raises(TypeError):
        power_of_product(1, '2', power=3)

@pytest.mark.parametrize('value1, value2, value3, expected', [(2, 2, 2, 16), (2, -2, 2, 16), (-2, 3, 2, 36),
                                                              (2, 2, 0.5, 2)])
def test_value_power_of_product(value1, value2, value3, expected):
    assert power_of_product(value1, value2, power=value3) == expected, 'not expected result'